package com.example.zenai;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.zenai.fragments.InfoFragment;
import com.example.zenai.fragments.LocationFragment;
import com.example.zenai.fragments.PoliceFragment;
import com.example.zenai.server.BMWServer;
import com.example.zenai.server.CronHandler;

public class MainActivityB extends Activity implements TabListener {

	private ActionBar.Tab location, police, info;
	private Fragment previousFragment;
	private LocationFragment mLocationFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mainb);
		
		// Launch Synchronization
	    new CronHandler(this).start();
	    mLocationFragment = new LocationFragment();
		mLocationFragment.setStatus(BMWServer.STATUS_OK);
		
		// Prepare UI		
		ActionBar bar = getActionBar();
	    bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	    
	    location = bar.newTab().setText("Location");
	    police = bar.newTab().setText("Police");
	    info = bar.newTab().setText("Info");
	    
	    location.setTabListener(this);
	    police.setTabListener(this);
	    info.setTabListener(this);
	    
	    bar.addTab(location);
	    bar.addTab(police);
	    bar.addTab(info);
	    
	}

	public void updateStatus(int status) {
		mLocationFragment.setStatus(status);
	}
	
	/* TabListener */
	
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		if (tab == location) {
			setFragment(mLocationFragment, ft);
		} else if (tab == police) {
			setFragment(new PoliceFragment(), ft);
		} else { // info
			setFragment(new InfoFragment(), ft);
		}
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {	
	}
	
	private void setFragment(Fragment fragment, FragmentTransaction ft) {
		if (previousFragment != null) {
			ft.remove(previousFragment);
		}
		ft.add(R.id.fragment_container, fragment);
		previousFragment = fragment;
	}
	
}
