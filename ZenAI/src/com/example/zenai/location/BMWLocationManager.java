package com.example.zenai.location;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class BMWLocationManager implements LocationListener {

	private Context mContext;
	private LocationManager mLocationManager;
	private Geocoder mGeoCoder;
	private BMWLocationListener mListener;
	
	public BMWLocationManager(Context context, BMWLocationListener listener) {
		this.mContext = context;
		this.mListener = listener;
		mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		mGeoCoder = new Geocoder(context);
		
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		mLocationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, this);
	}

	/* LocationListener */
	
	public void onLocationChanged(Location location) {
		double latitude = location.getLatitude(),
				longitude = location.getLongitude();
		String text;
		try {
			List<Address> addresses = mGeoCoder.getFromLocation(latitude, longitude, 1);
			Address address = addresses.get(0);
			text = "";
			String tmp;
			int i = 0;
			while ((tmp = address.getAddressLine(i++)) != null) {
				text += " " + tmp;
			}
		} catch (IOException e) {
			e.printStackTrace();
			text = "Location not found";
		}
		mListener.onLocationUpdated(latitude, longitude, text);
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	public interface BMWLocationListener {
		public void onLocationUpdated(double latitude, double longitude, String name);
	}
	
}
