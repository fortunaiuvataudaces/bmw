package com.example.zenai.fragments;

import java.net.URI;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.zenai.R;

public class PoliceFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.police, null);
		((Button) view.findViewById(R.id.false_alarm)).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				executeHttpGet();
			}
		});
		
		return view;
	}
	
	public void executeHttpGet() {
		Thread t = new Thread(new Runnable() {
			
			public void run() {
				try {
					HttpClient client = new DefaultHttpClient();
					HttpGet request = new HttpGet();
					request.setURI(new URI(
							"http://www.starcostudios.com/android/aewar/zencatserver/setStatus.php?userId=bmwdroid&status=ok"));
					client.execute(request);

				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
	}
	
}
