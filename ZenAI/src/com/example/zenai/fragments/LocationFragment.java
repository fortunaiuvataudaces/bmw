package com.example.zenai.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zenai.R;
import com.example.zenai.server.BMWServer;

public class LocationFragment extends Fragment {

	public final static String URL = "http://www.starcostudios.com/android/aewar/zencatserver/lonelyMap.php?userGet=bmwdroid&password=sss";
	
	private WebView mWebView;
	private TextView mStatusText;
	private int status;
	
	private boolean viewCreated = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		
		RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.location, null);
		
		mWebView = (WebView) view.findViewById(R.id.webview);
		mWebView.loadUrl(URL);
		mWebView.getSettings().setJavaScriptEnabled(true);
		
		mStatusText = (TextView) view.findViewById(R.id.status);
		
		viewCreated = true;
		setStatus(status);
		
		return view;
	}
	
    public void setStatus(int status) {
    	this.status = status;
    	
    	if (!viewCreated)
    		return;
    	
    	switch(status) {
    	case BMWServer.STATUS_OK:
    		mStatusText.setText("Status: OK");
    		mStatusText.setTextColor(Color.BLACK);
    		mStatusText.setBackgroundColor(getResources().getColor(R.color.status_ok));    		
    		break;
    	case BMWServer.STATUS_ALARM:
    		mStatusText.setText("Status: In Motion");
    		mStatusText.setTextColor(Color.WHITE);
    		mStatusText.setBackgroundColor(getResources().getColor(R.color.status_alert));
    		break;
    	}
    }
	
}
