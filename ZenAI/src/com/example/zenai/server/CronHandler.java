package com.example.zenai.server;

import android.os.Handler;
import android.os.Message;

import com.example.zenai.MainActivityB;

public class CronHandler extends Handler {

	private MainActivityB mMainActivity;
	private int lastStatus;
	public boolean looping;
	
	public CronHandler(MainActivityB activity) {
		this.mMainActivity = activity;
		lastStatus = -1;
	}
	
	public void start() {
		looping = true;
		postpone();		
	}
	
	public void stop() {
		looping = false;
	}
	
	public void postpone() {
		removeMessages(0);
		sendMessageDelayed(obtainMessage(0), BMWServer.REFRESH_RATE);
	}
	
	@Override
	public void handleMessage(Message msg) {
		if (looping) {
			BMWServer server = BMWServer.getServer();
			server.pull("Handler");
			int status = server.getStatus();
			
			System.out.println("NOEL-DEBUG: statuses " + status + ", " + lastStatus);
			if (status != lastStatus) {
				lastStatus = status;
				System.out.println("NOEL-DEBUG: Updating status to " + status);
				mMainActivity.updateStatus(lastStatus);
			}
			postpone();
		}
	}
	
}
