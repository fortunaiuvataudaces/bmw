package com.example.zenai.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.zenai.MainActivity;

public class CronReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		BMWServer server = BMWServer.getServer();
		server.pull("Alarm");
		
		if (server.getStatus() != BMWServer.STATUS_OK) {
			Intent newIntent = new Intent(context, MainActivity.class);
			newIntent.putExtra("status", server.getStatus());
			context.startActivity(intent);
		}
	}
	
}
