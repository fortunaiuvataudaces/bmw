package com.example.zenai.server;

import java.net.URI;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class BMWServer {

	public final static int REFRESH_RATE = 1000;

	public final static int STATUS_OK = 1;
	public final static int STATUS_ALARM = 2;

	private int status = STATUS_OK;
	private static BMWServer instance;
	
	private BMWServer() {}
	
	public static BMWServer getServer() {
		if (instance == null) {
			instance = new BMWServer();
		}
		return instance;
	}
	
	public void pull(String who) {
		System.out.println("NOEL-DEBUG: Pulling from " + who);
		executeHttpGet();
	}

	public int getStatus() {
		return status;
	}

	public void executeHttpGet() {
		Thread t = new Thread(new Runnable() {
			
			public void run() {
				try {
					HttpClient client = new DefaultHttpClient();
					HttpGet request = new HttpGet();
					request.setURI(new URI(
							"http://www.starcostudios.com/android/aewar/zencatserver/getStatus.php?userGet=bmwdroid&password=sss"));
					String response = EntityUtils.toString(client.execute(request).getEntity());

					status = response.trim().equalsIgnoreCase("ok") ? 
								STATUS_OK : STATUS_ALARM;
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
	}

}
