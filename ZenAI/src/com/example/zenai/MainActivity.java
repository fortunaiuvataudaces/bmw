package com.example.zenai;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zenai.location.BMWLocationManager;
import com.example.zenai.location.BMWLocationManager.BMWLocationListener;
import com.example.zenai.server.BMWServer;
import com.example.zenai.server.CronHandler;

public class MainActivity extends Activity implements BMWLocationListener {
	
	public final static String URL = "http://www.starcostudios.com/android/aewar/zencatserver/lonelyMap.php?userGet=bmwdroid&password=sss";
	
	private int status;
	
	private boolean isAlarmActivated;
	private TextView mStatusText, mLocationText;
	private Button mCallButton, mAlarmButton, mExpandButton;
	private WebView mWebView;
	
	private CronHandler cronHandler;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		startActivity(new Intent(this, MainActivityB.class));
		
		if (true) {
			return;
		}
		setUpCron();
		
		// Init Components
		mWebView = (WebView) findViewById(R.id.webview);
		mWebView.loadUrl(URL);
		mWebView.getSettings().setJavaScriptEnabled(true);
		
		mStatusText = (TextView) findViewById(R.id.status);
		mLocationText = (TextView) findViewById(R.id.location);
		
		mCallButton = (Button) findViewById(R.id.call);
		mAlarmButton = (Button) findViewById(R.id.alarm);
		mExpandButton = (Button) findViewById(R.id.expand);
		
		// Set Listeners
		mAlarmButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				toggleAlarm();
				updateAlarmButton();
			}
		});
		mCallButton.setOnClickListener(new OnClickListener() {			
			public void onClick(View v) {
				String url = "tel:636113108";
			    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
			    startActivity(intent);
			}
		});
		
		// Init application
		setStatus(BMWServer.STATUS_OK);
		
		// Init location manager
		new BMWLocationManager(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
    	if (item.getItemId() == R.id.menu_settings) {
    		Intent intent = new Intent(this, InfoActivity.class);
    		startActivity(intent);
    	}
    	return super.onMenuItemSelected(featureId, item);
    }
    
    public void setUpCron() {
		//Start Handler
		// Deprecated: cronHandler = new CronHandler(this);
		cronHandler.start();		
    }
    
    public void tearUpCron() {
    	// Stop Handler
    	cronHandler.stop();
    }

    public void toggleAlarm() {
    	isAlarmActivated = !isAlarmActivated;
    	if (isAlarmActivated) {
    		Toast.makeText(this, "Alarm Activated", Toast.LENGTH_SHORT).show();
    	} else {
    		Toast.makeText(this, "Alarm Deactivated", Toast.LENGTH_SHORT).show();
    	}
    }
    
    public void setStatus(int status) {
    	this.status = status;
    	switch(status) {
    	case BMWServer.STATUS_OK:
    		isAlarmActivated = false;
    		mStatusText.setText("Status: OK");
    		mStatusText.setTextColor(Color.BLACK);
    		mStatusText.setBackgroundColor(getResources().getColor(R.color.status_ok));    		
    		break;
    	case BMWServer.STATUS_ALARM:
    		isAlarmActivated = true;
    		mStatusText.setText("Status: In Motion");
    		mStatusText.setTextColor(Color.WHITE);
    		mStatusText.setBackgroundColor(getResources().getColor(R.color.status_alert));
    		break;
    	}
    	
    	updateCallButton();
    	updateAlarmButton();
    }
    
    public void updateCallButton() {
    	switch(status) {
    	case BMWServer.STATUS_OK:    		
    		mCallButton.setEnabled(false);
    		mCallButton.setBackground(getResources().getDrawable(R.drawable.disabled_button));
    		break;
    	case BMWServer.STATUS_ALARM:
    		mCallButton.setEnabled(true);
    		mCallButton.setBackground(getResources().getDrawable(R.drawable.ok_button));
    		break;
    	}
    }
    
    public void updateAlarmButton() {
    	if (isAlarmActivated) {
    		mAlarmButton.setBackground(getResources().getDrawable(R.drawable.alert_button));
    		mAlarmButton.setText("Deactivate Alarm");
    	} else {
    		mAlarmButton.setBackground(getResources().getDrawable(R.drawable.ok_button));
    		mAlarmButton.setText("Activate Alarm");
    	}
    }
    
    public void onLocationUpdated(double latitude, double longitude, String text) {
    	// TODO Modify this method
    	Toast.makeText(this, "current location: " + latitude + ", " + longitude, Toast.LENGTH_SHORT).show();
    	mLocationText.setText(text);
    }
    
}
